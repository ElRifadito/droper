extends RigidBody2D

const pop_power = 1.4
var fruta = load("res://fruta.tscn")
var lvl = 1
var taken = false
const grow_ratio = 1.18
var colors = [
Color(0.79215686, 0.12156863, 0.20392157),
Color(0.82941176, 0.24901961, 0.10196078),
Color(0.86666667, 0.37647059, 0.        ),
Color(0.90588235, 0.55294118, 0.00980392),
Color(0.94509804, 0.72941176, 0.01960784),
Color(0.9627451,  0.81372549, 0.03529412),
Color(0.98039216, 0.89803922, 0.05098039),
Color(0.79019608, 0.8745098,  0.25098039),
Color(0.6       , 0.85098039, 0.45098039),
Color(0.4627451,  0.80588235, 0.25490196),
Color(0.3254902 , 0.76078431, 0.05882353),
]

func _ready():
	if modulate == Color.WHITE:
		modulate = colors[0]

func start(lvl):
	#$CollisionShape.shape.radius *= lvl * grow_ratio
	self.lvl = lvl
	$LvLText.text = str(lvl)
	$LvLText.modulate = Color.BLACK
	set_size()
	mass = 3 - lvl/4
	remove_from_group("f1")
	add_to_group("f"+str(lvl))
	set_color()

func set_size():
	$CollisionShape.shape = $CollisionShape.shape.duplicate() 
	var grow_scale = grow_function(lvl, "cust")
	$CollisionShape.shape.radius *= grow_scale
	$Sprite.scale = Vector2(grow_scale, grow_scale)

func pop():
	$CollisionShape.shape.radius *= pop_power
	var t = Timer.new()
	add_child(t)
	t.wait_time = 0.05
	t.autostart = true
	t.one_shot = true
	t.connect("timeout", _depop)

func _depop():
	$CollisionShape.shape.radius /= pop_power
	
func set_color():
	if lvl == 1:
		modulate = colors[0]
	else:
		modulate = colors[lvl-1]
	
func _on_body_entered(body):
	if body.get_groups() == get_groups():		
		if body.taken == false and taken == false:
			taken = true
			body.taken = true
			self.queue_free()
			body.queue_free()
			
			var next = fruta.instantiate()
			next.start(lvl + 1)

			var pos1 = global_position
			var pos2 = body.global_position
			var between = pos1.lerp(pos2, 0.5);
			next.global_position = Vector2(between.x, between.y)
			
			get_parent().call_deferred("add_child", next)
			next.pop()

func grow_function(lvl, type="pow"):
	if type == "pow":
		return pow(lvl, grow_ratio)
	if type == "lin":
		return 1 + ((lvl-1) * grow_ratio)
	if type == "cust":
		var a = {
			1: 23,
			2: 32,
			3: 41,
			4: 50,
			5: 59,
			6: 74,
			7: 84,
			8: 94,
			9: 135,
			10: 150,
			11: 180,
			}
		return a[lvl]/12.0
