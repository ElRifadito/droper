extends Control

var fruta = preload("res://fruta.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	get_parent().find_child("Droper").connect("next_drop", _on_droper_next_drop)
	set_next_color(1)

func set_next_color(lvl):
	$Fruta.start(lvl)
	$Fruta/CollisionShape.shape.radius = 1

func _on_droper_next_drop(lvl):
	set_next_color(lvl)

